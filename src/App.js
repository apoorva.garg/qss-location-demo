import React from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/Navigation/MainNavigation';
import Locations from './components/Locations/Locations';
import AddLocation from './components/Locations/AddLocation/AddLocation';


const app = () => {
  return (
    <BrowserRouter>
      <React.Fragment>
        <MainNavigation />
        <main className="main-content">
          <Switch>
            <Redirect from="/" to="/locations" exact />
            <Route path="/locations" component={Locations} />
            <Route path="/add-location/:id" component={AddLocation}/>
            <Route path="/add-location" component={AddLocation} />
          </Switch>
        </main>
      </React.Fragment>
    </BrowserRouter>
  );
}

export default app;