import React, { useEffect, useState } from 'react'

import './Locations.css';

import ReactPaginate from 'react-paginate';

import Button from '../Button/Button';
import Location from './Location/Location';
import NoLocation from './NoLocation/NoLocation';

const Locations = props => {

    const [locations, setLocations] = useState([])
    const [pageCount, setPageCount] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [offset, setOffset] = useState(0)
    let perPage = 5

    const addLocation = () => {
        props.history.push('/add-location')
    }

    const getLocations = () => {
        const request = window.indexedDB.open('Locations', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('location', 'readwrite')
            const dataStore = transaction.objectStore('location')
            dataStore.getAll().onsuccess = function (event) {
                const result = event.target.result
                if (result && result.length > 0) {
                    setLocations(result)
                    setPageCount(Math.ceil(result.length / perPage))
                }
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('location', { keyPath: 'id' })
        }
    }

    useEffect(() => {
        getLocations()
    }, [])

    const deleteClickedHandler = ({ id }) => {
        const request = window.indexedDB.open('Locations', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('location', 'readwrite')
            const dataStore = transaction.objectStore('location')
            dataStore.delete(id).onsuccess = function (event) {
                dataStore.getAll().onsuccess = function (event) {
                    const result = event.target.result
                    if (result && result.length > 0) {
                        setLocations(result)
                    } else {
                        setLocations([])
                    }
                }
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('location', { keyPath: 'id' })
        }
    }

    const editClickedHandler = ({ id }) => {
        props.history.push(`/add-location/${id}`)
    }

    const handlePageClick = (e) => {
        console.log(e)
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setCurrentPage(selectedPage + 1)
        setOffset(offset)
        console.log(currentPage, perPage)
        getLocations()
    };


    let locationDisplay = <NoLocation />

    if (locations.length > 0) {
        const slice = locations.slice(offset, offset + perPage)
        locationDisplay =
            <React.Fragment>
                <div className="location">
                    <div className="location_header">
                        <p></p>
                        <p>Location Name</p>
                        <p>Address</p>
                        <p>Phone No.</p>
                        <p>Time Zone</p>
                        <p>Facility Times</p>
                        <p>Appointment Pool</p>
                    </div>
                </div>
                {
                    slice.map((location, index) => (
                        <span key={location.id}>
                            <Location
                                locationData={location}
                                index={perPage * (currentPage - 1) + index + 1}
                                deleteClicked={deleteClickedHandler}
                                editClicked={editClickedHandler} />
                        </span>
                    ))
                }
                <div className="location">
                    <div className="location_header" style={{ display: 'block' }}>
                        <ReactPaginate
                            previousLabel={"Prev"}
                            nextLabel={"Next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={handlePageClick}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"} />
                    </div>
                </div>
            </React.Fragment>
    }

    return (
        <div className="locations">
            <div className="locations_header">
                <p className="locations_title">Locations</p>
                <Button clicked={addLocation}>
                    <span className="fa fa-plus" style={{ marginRight: '10px' }}></span>
            Add Location</Button>
            </div>
            {locationDisplay}
        </div>
    );
}

export default Locations