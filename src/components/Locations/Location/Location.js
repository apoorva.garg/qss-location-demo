import React from 'react'

import './Location.css';

const location = props => {

    let phoneMatch = props.locationData.values.phone.match(/^(\d{3})(\d{3})(\d{4})$/)
    let formattedPhoneNumber = '(' + phoneMatch[1] + ') ' + phoneMatch[2] + '-' + phoneMatch[3]
    let appointment_pool = props.locationData.values.appointment_pool
    if (props.locationData.values.appointment_pool.includes(',')) {
        appointment_pool = <ul>
            {
                props.locationData.values.appointment_pool.split(',').map(item => (
                    <li key={item}>{item}</li>
                ))
            }
        </ul>
    }

    let timings = <ul>
        {
            props.locationData.values.timings.map(item => {
                let ele;
                if (item.checked) {
                    ele = <li key={item.id}>{item.value}({item.from}{item.zone.from} - {item.to}{item.zone.to})</li>
                }
                return ele
            })
        }
    </ul>
    return (
        <div className="location">
            <div className="location_details">
                <span className="location_index">{props.index}</span>
                <p>{props.locationData.values.location_name}</p>
                <p>{props.locationData.values.address_1},{props.locationData.values.address_2},{props.locationData.values.city},{props.locationData.values.state},{props.locationData.values.zip_code}</p>
                <p>{formattedPhoneNumber}</p>
                <p>{props.locationData.values.time_zone}</p>
                <span className="timing_class">{timings}</span>
                <span className="timing_class">{appointment_pool}</span>
                <span style={{ margin: '25px 0' }}>
                    <span className="fa fa-pencil" style={{ color: 'orange', marginRight: '20px', cursor: 'pointer' }} onClick={() => props.editClicked(props.locationData)}></span>
                    <span className="fa fa-trash" style={{ color: 'red', cursor: 'pointer' }} onClick={() => props.deleteClicked(props.locationData)}></span>
                </span>
            </div>
        </div>
    );
}

export default location