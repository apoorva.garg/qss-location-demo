import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'

import './AddLocation.css';

import Button from '../../Button/Button';
import Modal from '../../Modal/Modal';
import AddFacilityTimes from './AddFacilityTimes/AddFacilityTimes';

const AddLocation = props => {


    const { handleSubmit, register, errors, reset } = useForm();

    const [editForm, setEditForm] = useState({ location_name: '', address_1: '', address_2: '', appointment_pool: '', city: '', state: '', phone: '', suite_no: '', time_zone: '', zip_code: '', timings: [] })
    const [openModal, setOpenModal] = useState(false)
    const [timings, setTimings] = useState([])

    useEffect(() => {
        const { match: { params } } = props;
        if (Object.keys(params).length > 0) {
            const request = window.indexedDB.open('Locations', 1)
            request.onsuccess = function (event) {
                const db = event.target.result
                const transaction = db.transaction('location', 'readwrite')
                const dataStore = transaction.objectStore('location')
                dataStore.get(params.id).onsuccess = function (event) {
                    const result = event.target.result
                    if (Object.keys(result.length > 0)) {
                        setEditForm(result.values)
                    }
                }
            }
            request.onupgradeneeded = function (event) {
                const db = event.target.result
                db.createObjectStore('location', { keyPath: 'id' })
            }
        }
    }, [])

    const addLocationCancelHandler = () => {
        props.history.push('/locations')
    }

    const onSubmit = values => {
        const request = window.indexedDB.open('Locations', 1)
        request.onsuccess = function (event) {
            const db = event.target.result
            const transaction = db.transaction('location', 'readwrite')
            const dataStore = transaction.objectStore('location')

            let id = values.location_name
            values.timings = timings

            dataStore.put({ id, values }).onsuccess = function (event) {
                window.indexedDB.open('Locations', 1)
            }
        }
        request.onupgradeneeded = function (event) {
            const db = event.target.result
            db.createObjectStore('location', { keyPath: 'id' })
        }
        props.history.push('/locations')
        reset();
    }

    const handleSetLocationData = (e) => {
        const oldForm = { ...editForm }
        oldForm[e.target.name] = e.target.value
        setEditForm(oldForm)
    }

    const addFacilityTimeHandler = (e) => {
        setOpenModal(true);
    }

    const facilityTimesCancelHandler = (e) => {
        setOpenModal(false);
    }

    const saveTimeHandler = (timings) => {
        setTimings(timings)
        facilityTimesCancelHandler()
    }



    let timingsData =
        (
            <div className="halfWidth" style={{ marginTop: '45px', cursor: 'pointer' }} onClick={addFacilityTimeHandler}>
                <div className="fa fa-plus" style={{ color: '#2b5f8d', marginRight: '15px' }}></div>
                <span style={{ color: '#2b5f8d' }}>Add Facility Times<span className="astrik_color">*</span></span>
            </div>
        )

    const timingsDataShow = (data) => {
        timingsData = (
            <div className="halfWidth" style={{ marginTop: '25px' }}>
                <label className="Label">Timings<span className="astrik_color">*</span></label>
                <span style={{ display: 'flex' }}>
                    <ul>
                        {data.map(item => {
                            let ele;
                            if (item.checked) {
                                ele = <li key={item.id}>{item.value}({item.from}{item.zone.from} - {item.to}{item.zone.to})</li>
                            }
                            return ele
                        })
                        }
                    </ul>
                    <span className="fa fa-pencil" style={{ color: 'orange', marginLeft: '20px', cursor: 'pointer' }} onClick={addFacilityTimeHandler}></span>
                </span>
            </div>
        )
    }

    if (editForm.timings.length > 0) {
        timingsDataShow(editForm.timings)
    }

    if (!openModal && timings.length > 0) {
        timingsDataShow(timings)
    }

    return (
        <div className="add_location">
            <div className="add_location_details">
                <p className="add_location_title">Add Location</p>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="Input">
                        <label className="Label">Location Name<span className="astrik_color">*</span></label>
                        <input
                            label="Location Name"
                            type="text"
                            placeholder="Enter Location Name"
                            id="location_name"
                            value={editForm.location_name}
                            ref={register({ required: "Required" })}
                            onChange={(e) => handleSetLocationData(e)}
                            name="location_name"
                            className="InputElement"></input>
                        <p className="error_handler">{errors.location_name && errors.location_name.message}</p>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div className="Input halfWidth">
                            <label className="Label">Address Line 1<span className="astrik_color">*</span></label>
                            <input
                                label="Address Line 1"
                                type="text"
                                placeholder="Enter Address"
                                id="address_1"
                                value={editForm.address_1}
                                ref={register({ required: "Required" })}
                                className="InputElement"
                                onChange={(e) => handleSetLocationData(e)}
                                name="address_1"></input>
                            <p className="error_handler">{errors.address_1 && errors.address_1.message}</p>
                        </div>
                        <div className="Input halfWidth">
                            <label className="Label">Suite No.</label>
                            <input
                                label="Suite No."
                                type="text"
                                placeholder="Enter Suite No."
                                id="suite_no"
                                value={editForm.suite_no}
                                ref={register}
                                onChange={(e) => handleSetLocationData(e)}
                                className="InputElement"
                                name="suite_no"></input>
                        </div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div className="Input halfWidth">
                            <label className="Label">Address Line 2<span className="astrik_color">*</span></label>
                            <input
                                label="Address Line 2"
                                type="text"
                                placeholder="Enter Address"
                                id="address_2"
                                value={editForm.address_2}
                                ref={register({ required: "Required" })}
                                onChange={(e) => handleSetLocationData(e)}
                                className="InputElement"
                                name="address_2"></input>
                            <p className="error_handler">{errors.address_2 && errors.address_2.message}</p>
                        </div>
                        <div className="Input smallWidth">
                            <label className="Label">City<span className="astrik_color">*</span></label>
                            <input
                                label="City"
                                type="text"
                                placeholder="Enter City"
                                id="city"
                                value={editForm.city}
                                ref={register({ required: "Required" })}
                                className="InputElement"
                                onChange={(e) => handleSetLocationData(e)}
                                name="city"></input>
                            <p className="error_handler">{errors.city && errors.city.message}</p>
                        </div>
                        <div className="Input smallWidth">
                            <label className="Label">State<span className="astrik_color">*</span></label>
                            <input
                                label="State"
                                type="text"
                                placeholder="Enter State"
                                id="state"
                                value={editForm.state}
                                ref={register({ required: "Required" })}
                                onChange={(e) => handleSetLocationData(e)}
                                className="InputElement"
                                name="state" ></input>
                            <p className="error_handler">{errors.state && errors.state.message}</p>
                        </div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div className="Input smallWidth">
                            <label className="Label">ZIP Code<span className="astrik_color">*</span></label>
                            <input
                                label="ZIP Code"
                                type="text"
                                placeholder="Enter ZIP Code"
                                id="zip_code"
                                onChange={(e) => handleSetLocationData(e)}
                                value={editForm.zip_code}
                                ref={register({
                                    required: "Required",
                                    maxLength: { value: 10, message: "ZIP length should be between 5 - 10" },
                                    minLength: { value: 5, message: "ZIP length should be between 5 - 10" },
                                    pattern: { value: /^\d+$/, message: "Only Numbers are allowed" }
                                },
                                )}
                                className="InputElement"
                                name="zip_code" ></input>
                            <p className="error_handler">{errors.zip_code && errors.zip_code.message}</p>
                        </div>
                        <div className="Input smallWidth">
                            <label className="Label">Phone Number<span className="astrik_color">*</span></label>
                            <input
                                label="Phone Number"
                                type="text"
                                placeholder="Enter Phone Number"
                                id="phone"
                                onChange={(e) => handleSetLocationData(e)}
                                value={editForm.phone}
                                ref={register({
                                    required: "Required",
                                    maxLength: { value: 10, message: "Enter 10 digit Phone Number" },
                                    minLength: { value: 10, message: "Enter 10 digit Phone Number" },
                                    pattern: { value: /^\d+$/, message: "Only Numbers are allowed" }
                                })}
                                className="InputElement"
                                name="phone"></input>
                            <p className="error_handler">{errors.phone && errors.phone.message}</p>
                        </div>
                        <div className="Input halfWidth">
                            <label className="Label">Time Zone</label>
                            <input
                                label="Time Zone"
                                type="text"
                                placeholder="Enter Time Zone"
                                id="time_zone"
                                ref={register}
                                onChange={(e) => handleSetLocationData(e)}
                                value={editForm.time_zone}
                                className="InputElement"
                                name="time_zone"></input>
                        </div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div className="Input halfWidth">
                            <label className="Label">Appointment Pool</label>
                            <input
                                label="Appointment Pool"
                                type="text"
                                placeholder="Enter Appointment Pool"
                                id="appointment_pool"
                                ref={register}
                                onChange={(e) => handleSetLocationData(e)}
                                value={editForm.appointment_pool}
                                className="InputElement halfWidth"
                                name="appointment_pool"></input>
                        </div>
                        {timingsData}
                    </div>
                    <div className="button_handler">
                        <Button clicked={addLocationCancelHandler} type="button" btnType="reset_button">Cancel</Button>
                        <Button type="submit" btnType="save_button">Save</Button>
                    </div>
                    {openModal && <Modal show={openModal} modalClosed={facilityTimesCancelHandler}>
                        <AddFacilityTimes modalClosedHandler={facilityTimesCancelHandler} saveTimeHandler={saveTimeHandler} editTimings={editForm.timings} setTimings={timings} />
                    </Modal>}
                </form>
            </div>
        </div>
    );
}

export default AddLocation