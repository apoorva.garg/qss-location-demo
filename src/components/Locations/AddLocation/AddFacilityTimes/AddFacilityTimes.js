import React, { useState } from 'react'

import './AddFacilityTimes.css';

import Button from '../../../Button/Button'

const AddFacilityTimes = props => {

    let defaultDays = [
        { id: 'sun', value: "Sun", checked: false, label: "Sun", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'mon', value: "Mon", checked: false, label: "Mon", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'tue', value: "Tue", checked: false, label: "Tue", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'wed', value: "Wed", checked: false, label: "Wed", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'thu', value: "Thu", checked: false, label: "Thu", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'fri', value: "Fri", checked: false, label: "Fri", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
        { id: 'sat', value: "Sat", checked: false, label: "Sat", from: '', to: '', zone: { from: 'AM', to: 'PM' }, classes: { from: { am: ['timezone'], pm: ['timezone'] }, to: { am: ['timezone'], pm: ['timezone'] } } },
    ]

    if (props.setTimings.length > 0) {
        defaultDays = props.setTimings
    }

    if (props.editTimings.length > 0) {
        defaultDays = props.editTimings
    }
    
    const [daysChecked, setDaysChecked] = useState(defaultDays)

    const checkedAllHandler = (day) => {
        const array = [...daysChecked]
        array.map(ele => {
            if (day.checked) {
                if (ele.checked) {
                    ele.checked = day.checked
                    ele.from = day.from
                    ele.to = day.to
                    ele.zone = day.zone
                    ele.classes = day.classes
                }
            }
            return ele
        })
        setDaysChecked(array)
    }

    const toggleCheckBoxHandler = (e) => {
        const array = [...daysChecked]
        array.map(day => {
            if (day.id === e.target.id) {
                day.checked = !day.checked
            }
            return day
        })
        setDaysChecked(array)
    }


    const checkFromTimeZone = (timing) => {
        timing.map(day => {
            if (day.checked) {
                const fromHour = day.from.split(":")[0] || "";
                const fromMinute = day.from.split(":")[1] || "";
                if (fromHour !== "" && Number(fromHour) >= 12) {
                    let newHour = fromHour - 12;
                    day.zone.from = 'PM'
                    day.classes.from.am = ['timezone']
                    day.classes.from.pm = ['timezone timezone_PM']
                    day.from = '0' + newHour + ':' + fromMinute
                }
                if (fromHour !== "" && Number(fromHour) < 12) {
                    day.zone.from = 'AM'
                    day.classes.from.am = ['timezone timezone_AM']
                    day.classes.from.pm = ['timezone']
                    day.from = fromHour + ':' + fromMinute
                }
            }
            return day
        })
        setDaysChecked(timing)
    }


    const checkToTimeZone = (timing) => {
        timing.map(day => {
            if (day.checked) {
                const toHour = day.to.split(":")[0] || "";
                const toMinute = day.to.split(":")[1] || "";
                if (toHour !== "" && Number(toHour) >= 12) {
                    let newHour = toHour - 12;
                    day.zone.to = 'PM'
                    day.classes.to.am = ['timezone']
                    day.classes.to.pm = ['timezone timezone_PM']
                    day.to = '0' + newHour + ':' + toMinute
                }
                if (toHour !== "" && Number(toHour) < 12) {
                    day.zone.to = 'AM'
                    day.classes.to.am = ['timezone timezone_AM']
                    day.classes.to.pm = ['timezone']
                    day.to = toHour + ':' + toMinute
                }
            }
            return day
        })
        setDaysChecked(timing)
    }


    const setTimeHandler = (e) => {
        const array = [...daysChecked]
        array.map(day => {
            if (day.id === e.target.id && e.target.name === 'from') {
                day.from = e.target.value
            } else if (day.id === e.target.id && e.target.name === 'to') {
                day.to = e.target.value
            }
            return day
        })
        if (e.target.name === 'from') {
            checkFromTimeZone(array)
        } else if (e.target.name === 'to') {
            checkToTimeZone(array)
        }
    }

    daysChecked.map(day => {
        if (day.zone.from === 'AM') {
            day.classes.from.am.push('timezone_AM')
        } else if (day.zone.from === 'PM') {
            day.classes.from.pm.push('timezone_PM')
        }
        if (day.zone.to === 'AM') {
            day.classes.to.am.push('timezone_AM')
        } else if (day.zone.to === 'PM') {
            day.classes.to.pm.push('timezone_PM')
        }
        return day
    })

    return (
        <div className="add_facility">
            <p className="add_facility_title">Facility Times</p>
            <div className="add_facility_content">
                <span></span>
                <span style={{ fontWeight: 'bold' }}>From</span>
                <span style={{ fontWeight: 'bold' }}>To</span>
            </div>
            {
                daysChecked.map(day => (
                    <div key={day.id} className="add_facility_content">
                        <span>
                            <input type="checkbox" id={day.id} name={day.value} value={day.value} checked={day.checked} onChange={toggleCheckBoxHandler} />
                            <label htmlFor={day.id}>{day.label}</label>
                        </span>
                        <div>
                            <input id={day.id} type="time" placeholder="HH:MM" value={day.from} name="from" onChange={(e) => setTimeHandler(e)} className="time_zone_input" disabled={!day.checked} />
                            <div style={{ display: 'inline-block', marginLeft: '20px' }}>
                                <Button type="button" btnType={day.classes.from.am.join(' ')}>AM</Button>
                                <Button type="button" btnType={day.classes.from.pm.join(' ')}>PM</Button>
                            </div>
                        </div>
                        <div>
                            <input id={day.id} type="time" placeholder="HH:MM" value={day.to} name="to" onChange={(e) => setTimeHandler(e)} className="time_zone_input" disabled={!day.checked} />
                            <div style={{ display: 'inline-block', marginLeft: '20px' }}>
                                <Button type="button" btnType={day.classes.to.am.join(' ')}>AM</Button>
                                <Button type="button" btnType={day.classes.to.pm.join(' ')}>PM</Button>
                            </div>
                        </div>
                        <Button clicked={() => checkedAllHandler(day)} type="button" btnType="checked_button">Apply to All Checked</Button>
                    </div>
                ))
            }
            <div className="button_handler">
                <Button clicked={props.modalClosedHandler} type="button" btnType="reset_button">Cancel</Button>
                <Button type="buttion" btnType="save_button" clicked={() => props.saveTimeHandler(daysChecked)}>Save</Button>
            </div>
        </div>
    );
}

export default AddFacilityTimes