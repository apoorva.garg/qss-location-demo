import React from 'react'

import './NoLocation.css'

const noLocation = props => {
    return (
        <div className="no_location">
            <div className="no_location_sign">
                <span className="fa fa-map-marker" style={{ fontSize: '150px' }}></span>
            </div>
            <p className="no_location_header">Kindly Add Your Location First</p>
            <p className="no_location_title">There is no location added right now</p>
        </div>
    );
}

export default noLocation