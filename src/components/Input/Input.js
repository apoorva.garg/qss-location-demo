import React from 'react'

import './Input.css';

const input = (props) => {
    let classes = ['Label', [props.styleType]].join(' ')
    console.log(props)
    return (
        <div className={classes}>
            <label className="Label">{props.label}</label>
            <input className="InputElement" {...props} ref={props.refel} />
        </div>
    );
}

export default input